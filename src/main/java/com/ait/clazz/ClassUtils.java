package com.ait.clazz;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ait.Filter;
import com.ait.string.StringUtils;

public class ClassUtils {

	private static final String DEBUG = "----> DEBUG - ";
	private static Integer JVM_FACTOR = null;

	/**
	 * Prints the message prefixed with {@link ClassUtils#DEBUG}, class and method
	 * name.<br>
	 * 
	 * @see ClassUtils#getClassName()
	 * @see ClassUtils#getMethodName()
	 */
	public static void debug(String message) {
		System.out.println(DEBUG + getClassName() + "." + getMethodName() + ":"
				+ message);
	}

	/**
	 * Returns the name of the current executed method.<br>
	 * <br>
	 * <b>There is following pitfall:</b><br>
	 * Some virtual machines may, under some circumstances, omit one or more stack
	 * frames from the stack trace. In the extreme case, a virtual machine that
	 * has no stack trace information concerning this thread is permitted to
	 * return a zero-length array from this method.
	 * 
	 * @see ClassUtils#getMethodName(int)
	 * @return the name
	 */
	public static String getMethodName() {
		return getMethodName(1);
	}

	/**
	 * @param depth
	 *          the depth of the method in the stack trace.<br/>
	 *          0 is the innermost method, and ste.length is the outermost. if you
	 *          give a number below 0 (e.g. -1) the method will return the element
	 *          at StackTrace.length - your number.
	 * @see ClassUtils#getStackTraceElement(int)
	 * @return the name
	 */
	public static String getMethodName(final int depth) {
		return getStackTraceElement(depth).getMethodName();
	}

	/**
	 * Returns the name of the class which method is currently executed.<br>
	 * <br>
	 * <b>There is following pitfall:</b><br>
	 * Some virtual machines may, under some circumstances, omit one or more stack
	 * frames from the stack trace. In the extreme case, a virtual machine that
	 * has no stack trace information concerning this thread is permitted to
	 * return a zero-length array from this method.
	 * 
	 * @see ClassUtils#getClassName(int)
	 * @return the name
	 */
	public static String getClassName() {
		return getClassName(1);
	}

	/**
	 * @param depth
	 *          the depth of the method in the stack trace.<br/>
	 *          0 is the innermost method, and ste.length is the outermost. if you
	 *          give a number below 0 (e.g. -1) the method will return the element
	 *          at StackTrace.length - your number.
	 * @see ClassUtils#getStackTraceElement(int)
	 * @return the name
	 */
	public static String getClassName(final int depth) {
		return getStackTraceElement(depth).getClassName();
	}

	/**
	 * @param depth
	 *          the depth of the method in the stack trace.<br/>
	 *          0 is the innermost method, and ste.length is the outermost. if you
	 *          give a number below 0 (e.g. -1) the method will return the element
	 *          at StackTrace.length - your number.
	 * @return {@link StackTraceElement}
	 */
	public static StackTraceElement getStackTraceElement(final int depth) {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

		int index = depth + getJvmFactor();
		int length = ste.length;
		if (depth < 0 && depth > length * -1) {
			index = length + depth;
		}
		return ste[index];
	}

	private static final Integer getJvmFactor() {
		if (JVM_FACTOR == null) {
			boolean found = false;
			int i = 0;
			final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
			for (; i < ste.length && !found; i++) {
				if ("getJvmFactor".equals(ste[i].getMethodName())) {
					found = true;
				}
			}
			if (!found) {
				JVM_FACTOR = 0;
			} else {
				JVM_FACTOR = i + 1;
			}
		}
		return JVM_FACTOR;
	}

	public static Method findGetterMethod(Object o1, String propertyName) {
		String ucFirstPropertyName = StringUtils.upperFirstChar(propertyName);
		List<String> list = Arrays.asList(new String[] { propertyName,
				"get" + ucFirstPropertyName, "is" + ucFirstPropertyName,
				"has" + ucFirstPropertyName });
		return ClassUtils.findMethod(o1, list);
	}

	public static Method findMethod(Object o1, List<String> possibleNames) {
		List<Method> methods = getAllMethods(o1, new GetterFilter(possibleNames));
		if (methods != null && methods.size() > 0) {
			return methods.get(0);
		}
		return null;
	}

	public static List<Method> getAllGetterMethods(Object o1) {
		return getAllMethods(o1, new GetterFilter());
	}

	public static class GetterFilter implements Filter<Method> {

		private List<String> possibleNames;

		public GetterFilter() {
			this(null);
		}

		public GetterFilter(List<String> possibleNames) {
			this.possibleNames = possibleNames;
		}

		public boolean isInFilter(Method item) {
			return item.getParameterTypes().length == 0
					&& (possibleNames == null || possibleNames.contains(item.getName()));
		}
	}

	public static List<Method> getAllMethods(Object o1, Filter<Method> filter) {
		Class<? extends Object> class1 = o1.getClass();
		Method[] methods = class1.getMethods();
		List<Method> result = new ArrayList<Method>();
		for (Method item : methods) {
			if (filter == null || filter.isInFilter(item)) {
				result.add(item);
			}
		}
		return result;
	}

}
