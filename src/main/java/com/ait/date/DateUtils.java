package com.ait.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

	/**
	 * Default format is dd.mm.yyyy
	 */
	public static final String DEFAULT_FORMAT = "dd.MM.yyyy";
	public static final String DEFAULT_FORMAT_UTC = "yyyy-MM-dd";
	public static final String DAY_AS_PARAM_FORMAT = "yyyyMMdd";
	public static final String YEAR_FORMAT = "yyyy";
	public static final String MONTH_YEAR_FORMAT = "MMMMM yyyy";
	public static final String DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
	private static final Locale DEFAULT_LOCALE = new Locale("de", "AT");

	public static Calendar newCalendar(final Calendar date) {
		if (date != null) {
			return newCalendar(date.getTime());
		}
		return null;
	}

	public static Calendar newCalendar(final Date date) {
		if (date != null) {
			return newCalendar(date.getTime());
		}
		return null;
	}

	public static Calendar newCalendar(final long timeInMillis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timeInMillis);
		return calendar;
	}

	public static Calendar addDays(final Calendar date, final int amount) {
		return addDays(date.getTime(), amount);
	}

	public static Calendar addDays(final Date date, final int amount) {
		return add(date, Calendar.DATE, amount);
	}

	public static Calendar addMonths(final Calendar date, final int amount) {
		return addMonths(date.getTime(), amount);
	}

	public static Calendar addMonths(final Date date, final int amount) {
		return add(date, Calendar.MONTH, amount);
	}

	public static Calendar addYears(final Calendar date, final int amount) {
		return addYears(date.getTime(), amount);
	}

	public static Calendar addYears(final Date date, final int amount) {
		return add(date, Calendar.YEAR, amount);
	}

	public static Calendar add(final Date date, final int field, final int amount) {
		Calendar calendar = newCalendar(date);
		calendar.add(field, amount);
		return calendar;
	}

	public static Calendar getStartOfYear(final Calendar date) {
		return getStartOfYear(date.getTime());
	}

	public static Calendar getStartOfYear(final Date date) {
		Calendar calendar = getStartOfMonth(date);
		calendar.set(Calendar.MONTH, 0);
		return calendar;
	}

	public static Calendar getEndOfYear(final Calendar date) {
		return getEndOfYear(date.getTime());
	}

	public static Calendar getEndOfYear(final Date date) {
		Calendar calendar = newCalendar(date);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DATE, 31);
		return calendar;
	}

	public static Calendar getStartOfMonth(final Calendar date) {
		return getStartOfMonth(date.getTime());
	}

	public static Calendar getStartOfMonth(final Date date) {
		Calendar calendar = getStartOfDay(date);
		calendar.set(Calendar.DATE, 1);
		return calendar;
	}

	public static Calendar getEndOfMonth(final Calendar date) {
		return getEndOfMonth(date.getTime());
	}

	public static Calendar getEndOfMonth(final Date date) {
		Calendar calendar = getStartOfMonth(date);
		calendar = addMonths(calendar, 1);
		calendar = addDays(calendar, -1);
		return getEndOfDay(calendar);
	}

	/**
	 * @return the start of the day (eg: 17.01.2014 0:00:00)
	 */
	public static Calendar getStartOfDay(long dateTime) {
		Calendar calendar = newCalendar(dateTime);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * @return the start of the day (eg: 17.01.2014 0:00:00)
	 */
	public static Calendar getStartOfDay(final Date dateTime) {
		return getStartOfDay(dateTime.getTime());
	}

	/**
	 * @return the end of the day (eg: 17.01.2014 23:59:59.999)
	 */
	public static Calendar getEndOfDay(final Calendar dateTime) {
		return getEndOfDay(dateTime.getTimeInMillis());
	}

	/**
	 * @return the end of the day (eg: 17.01.2014 23:59:59.999)
	 */
	public static Calendar getEndOfDay(long dateTime) {
		Calendar calendar = newCalendar(dateTime);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar;
	}

	/**
	 * @return the end of the day (eg: 17.01.2014 23:59:59.999)
	 */
	public static Calendar getEndOfDay(final Date dateTime) {
		return getEndOfDay(dateTime.getTime());
	}

	/**
	 * @return the start of the day (eg: 17.01.2014 0:00:00)
	 */
	public static Calendar getStartOfDay(final Calendar dateTime) {
		return getStartOfDay(dateTime.getTimeInMillis());
	}

	public static java.sql.Date toSqlDate(final Calendar date) {
		return new java.sql.Date(date.getTimeInMillis());
	}

	public static java.sql.Date toSqlDate(final Date value) {
		if (value == null) {
			return null;
		}
		return new java.sql.Date(value.getTime());
	}

	/**
	 * @return the start of today (eg: 17.01.2014 0:00:00)
	 */
	public static Calendar today() {
		Calendar today = Calendar.getInstance();
		return getStartOfDay(today);
	}

	/**
	 * Returns the date like the default format {@link DateUtils#DEFAULT_FORMAT}
	 * 
	 * @param date
	 * @return
	 */
	public static String getFormattedDate(final Calendar date) {
		if (date != null) {
			return getFormattedDate(date, DEFAULT_FORMAT);
		}
		return null;
	}

	/**
	 * Returns the date like the default format {@link DateUtils#DEFAULT_FORMAT}
	 * 
	 * @param date
	 * @return
	 */
	public static String getFormattedDate(final Date date) {
		if (date != null) {
			return getFormattedDate(date, DEFAULT_FORMAT);
		}
		return null;
	}

	/**
	 * Returns the date like the default format {@link DateUtils#DEFAULT_FORMAT}
	 * 
	 * @param date
	 * @return
	 */
	public static String getFormattedDate(final Date date, final String format) {
		if (date != null) {
			return new SimpleDateFormat(format, DEFAULT_LOCALE).format(date);
		}
		return null;
	}

	/**
	 * Returns the date like the given format
	 * 
	 * @param date
	 * @return
	 */
	public static String getFormattedDate(final Calendar date, final String format) {
		if (date != null) {
			return getFormattedDate(date.getTime(), format);
		}
		return null;
	}

	public static Date parseDate(String dateString) {
		Date result = parseDate(dateString, DEFAULT_FORMAT);
		if (result == null) {
			result = parseDate(dateString, DEFAULT_FORMAT_UTC);
		}
		if (result == null) {
			result = parseDate(dateString, YEAR_FORMAT);
		}
		if (result == null) {
			result = parseDate(dateString, MONTH_YEAR_FORMAT);
		}
		return result;
	}

	public static Date parseDate(String dateString, String format) {
		if (dateString != null && format != null && dateString.length() == format.length()) {
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
				Calendar calendar = Calendar.getInstance();
				calendar.setLenient(false);
				simpleDateFormat.setCalendar(calendar);
				return simpleDateFormat.parse(dateString);
			} catch (ParseException e) {
				return null;
			}
		}
		return null;
	}

	public static boolean isInPeriod(Date date, Date from, Date to) {
		return (from == null || !date.before(from))
				&& (to == null || !date.after(to));
	}

	public static Calendar convertToTimezone(long date, TimeZone fromTimezone,
			TimeZone toTimezone) {
		Calendar result = DateUtils.newCalendar(date);
		// from fromTimezone to GMT
		result.add(Calendar.MILLISECOND, fromTimezone.getOffset(date));
		// from GMT to toTimezone
		result.add(Calendar.MILLISECOND, toTimezone.getOffset(date));
		return result;
	}

	public static Calendar convertToTimezone(Date date, TimeZone from, TimeZone to) {
		return convertToTimezone(date.getTime(), from, to);
	}

	public static Calendar convertToTimezone(Calendar date, TimeZone from,
			TimeZone to) {
		return convertToTimezone(date.getTimeInMillis(), from, to);
	}

	/**
	 * Example: {@link PakCalendarUtils#calculateXDayOfMonth("Wed, 19.03.2014", 1,
	 * Calendar.MONDAY)} results in the date <tt>"Mon, 24.03.2014"</tt>.
	 * 
	 * @param date
	 *          the date
	 * @param oneStep
	 *          +1 or -1 (for forward or backward search)
	 * @param day
	 *          e.g <tt>Calendar.MONDAY</tt>
	 * 
	 * @return
	 */
	protected static Date calculateXDayOfWeek(Date date, int oneStep, int day) {
		java.util.Calendar c = DateUtils.getStartOfDay(date);

		// search until correct day of week (e.g. Monday)
		while (c.get(java.util.Calendar.DAY_OF_WEEK) != day) {
			c.add(java.util.Calendar.DAY_OF_MONTH, oneStep);
		}
		return c.getTime();
	}

	public static int compareDays(Date date1, Date date2) {
		return compareDates(date1, date2, Calendar.YEAR, Calendar.MONTH,
				Calendar.DAY_OF_MONTH);
	}

	protected static int compareDates(Date date1, Date date2, int... fields) {
		if (date1 == date2) {
			return 0;
		} else if (date1 == null) {
			return -1;
		} else if (date2 == null) {
			return 1;
		} else if (fields != null && fields.length > 0) {
			Calendar c1 = newCalendar(date1);
			Calendar c2 = newCalendar(date2);
			for (int field : fields) {
				int i1 = c1.get(field);
				int i2 = c2.get(field);
				if (i1 != i2) {
					return new Integer(i1).compareTo(new Integer(i2));
				}
			}
		}
		return 0;
	}

	public static String getQuartal(Date date) {
		return Integer.toString((DateUtils.newCalendar(date).get(Calendar.MONTH) + 3) / 3);
	}

	public static Calendar getStartOfQuartal(Date date) {
		Calendar c = getStartOfMonth(date);
		int month = c.get(Calendar.MONTH);
		while (month != Calendar.JANUARY && month != Calendar.APRIL
				&& month != Calendar.JULY && month != Calendar.OCTOBER) {
			c.add(Calendar.MONTH, -1);
			month = c.get(Calendar.MONTH);
		}
		return c;
	}

	public static Calendar getEndOfQuartal(Date date) {
		Calendar c = getEndOfMonth(date);
		int month = c.get(Calendar.MONTH);
		while (month != Calendar.MARCH && month != Calendar.JUNE
				&& month != Calendar.SEPTEMBER && month != Calendar.DECEMBER) {
			c.add(Calendar.MONTH, 1);
			month = c.get(Calendar.MONTH);
		}
		return c;
	}
}
