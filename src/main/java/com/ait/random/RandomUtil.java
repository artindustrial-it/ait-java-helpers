package com.ait.random;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomUtil {

	public static <T> T getRandom(List<T> list) {
		T item = null;
		if (list != null && list.size() > 0) {
			item = list.get(getRandomNumber(list.size()));
		}
		return item;
	}

	public static <T> T getRandom(T... list) {
		return getRandom(Arrays.asList(list));
	}

	public static int getRandomNumber(int max) {
		return getRandomNumber(0, max);
	}

	public static int getRandomNumber(int min, int max) {
		int spread = max - min;
		if (spread < 0) {
			spread *= -1;
		} else if (spread == 0) {
			throw new IllegalArgumentException(
					"Min und Max dürfen nicht gleich sein.");
		}
		return new Random(getSeed()).nextInt(max - min) + min;
	}

	private static long getSeed() {
		return System.currentTimeMillis();
	}
}
