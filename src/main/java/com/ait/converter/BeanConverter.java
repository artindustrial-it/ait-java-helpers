package com.ait.converter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.ait.clazz.ClassUtils;

public class BeanConverter<F extends Object> implements Converter<F, String> {

	private Converter<F, ?> innerConverter;
	private String property;

	public BeanConverter(String property) {
		this.property = property;
	}

	public BeanConverter(String property, Converter<F, ?> innerConverter) {
		this(property);
		this.innerConverter = innerConverter;
	}

	public String convert(F object) {
		if (object != null) {
			Object o = object;
			if (innerConverter != null) {
				o = innerConverter.convert(object);
			}
			Method getterMethod = ClassUtils.findGetterMethod(object, property);
			try {
				return getterMethod.invoke(object).toString();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}