package com.ait.converter;

public interface DualConverter<F, T> {

	T convertFromDBObject(F object);

	F convertToDBObject(T object);

	F convertFromHumanReadable(String object);
	
	String convertToHumanReadable(F object);
}