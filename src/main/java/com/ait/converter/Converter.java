package com.ait.converter;

public interface Converter<F, T> {
	T convert(F object);
}