package com.ait.converter;

import java.util.ArrayList;
import java.util.List;

public class ConverterUtils {

	public static <F, T> List<T> convert(List<F> list, Converter<F, T> converter) {
		List<T> result = null;
		if (list != null) {
			result = new ArrayList<T>();
			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					T converted = converter.convert(list.get(i));
					if (converted != null) {
						result.add(converted);
					}
				}
			}
		}
		return result;
	}

	public static <F, T> List<T> toObject(List<F> list, DualConverter<F, T> converter) {
		return _convertObject(list, converter, true);
	}

	public static <F, T> List<T> fromObject(List<F> list, DualConverter<F, T> converter) {
		return _convertObject(list, converter, false);
	}
	
	private static List _convertObject(List list, DualConverter converter, boolean toObject) {
		List result = null;
		if (list != null) {
			result = new ArrayList();
			if (!list.isEmpty()) {
				for (int i = 0; i < list.size(); i++) {
					if (toObject) {
						result.add(converter.convertToDBObject(list.get(i)));
					} else {
						result.add(converter.convertFromDBObject(list.get(i)));
					}
				}
			}
		}
		return result;
	}
}
