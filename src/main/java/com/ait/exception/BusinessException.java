package com.ait.exception;

public class BusinessException extends Exception {

	public BusinessException(String message) {
		super(message);
	}

}
