package com.ait.exception;

public class ExceptionUtils {

	public static String getInnermostMessage(Throwable t) {
		Throwable innermost = getInnermostThrowable(t);
		if (innermost == null) {
			return null;
		}
		return innermost.getMessage();
	}

	public static Throwable getInnermostThrowable(Throwable t) {
		if (t == null) {
			return null;
		}
		while (t.getCause() != null) {
			return getInnermostThrowable(t.getCause());
		}
		return t;
	}
}
