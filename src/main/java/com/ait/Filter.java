package com.ait;

public interface Filter<T> {

	public boolean isInFilter(T o);
}
