package com.ait.string;

public class HashableException extends Exception {
	public HashableException(Hashable object) {
		super("Salt could not be created because object '" + object + "' returned no valid hash.");
	}
}
