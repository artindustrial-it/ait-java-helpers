package com.ait.string;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordUtil {

	private static final String DEFAULT_ENCODING = "UTF-8";

	public static String getHash(Hashable objectForSeed, String password)
			throws NoSuchAlgorithmException, UnsupportedEncodingException,
			HashableException {
		String salt = objectForSeed.getHash();
		if (salt == null) {
			throw new HashableException(objectForSeed);
		}
		return getHash(salt, password);
	}

	public static String getHash(Integer salt, String password)
			throws HashableException, UnsupportedEncodingException,
			NoSuchAlgorithmException {
		return getHash(salt.toString(), password);
	}
	
	public static String getHash(String salt, String password)
			throws HashableException, UnsupportedEncodingException,
			NoSuchAlgorithmException {
		if (salt == null) {
			throw new IllegalArgumentException("Salt must not be null or empty.");
		}
		return new String(getHash(5, password, salt.getBytes()), DEFAULT_ENCODING);
	}

	protected static byte[] getHash(int iterationNb, String password, byte[] salt)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		digest.reset();
		digest.update(salt);
		byte[] input = digest.digest(password.getBytes(DEFAULT_ENCODING));
		for (int i = 0; i < iterationNb; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return input;
	}
}
