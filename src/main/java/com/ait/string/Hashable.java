package com.ait.string;

public interface Hashable {
	/**
	 * This function returns a hash value from the object.<br/>
	 * If two objects are representing the same / identical domain object, they
	 * MUST return the same hash value.<br>
	 * e.g.<br>
	 * <code>User@ase45as34f: name: Max Mustermann, id: 1</code>
	 * --> returns 'ASDF'<br>
	 * <code>User@a7f789csdv: name: Max Mustermann, id: 1</code>
	 * --> must also return 'ASDF'.
	 * 
	 * @return the hash value of an object (must be repeatable with the same result)
	 */
	public String getHash();
}
