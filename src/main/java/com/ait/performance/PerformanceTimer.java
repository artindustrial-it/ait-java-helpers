package com.ait.performance;

import com.ait.clazz.ClassUtils;

public class PerformanceTimer {
	private Long startTime;
	private Long stopTime;
	private String functionIdentifier;
	
	public PerformanceTimer() {
		this (null);
	}
	
	public PerformanceTimer(String functionIdentifier) {
		this.functionIdentifier = functionIdentifier;
		startTime = null;
	}
	
	public PerformanceTimer start() {
		startTime = System.currentTimeMillis();
		if (getFunctionIdentifier() == null) {
			String id = createDefaultFunctionIdentifier();
			setFunctionIdentifier(id);
		}
		return this;
	}
	
	protected String createDefaultFunctionIdentifier() {
		return ClassUtils.getClassName(1) + "." + ClassUtils.getMethodName(2);
	}

	protected static String formatTime(long milliseconds) {
		long temp = milliseconds;
		int milli = (int) (temp % 1000);
		temp = temp / 1000;
		int seconds = (int) (temp % 60);
		temp = temp / 60;
		int minutes = (int) (temp % 60);
		int hours   = (int) temp / 60;
		return formatNumber(hours, "%02d") 
				+ ":" + formatNumber(minutes, "%02d") 
				+ ":" + formatNumber(seconds, "%02d") 
				+ "." + formatNumber(milli, "%04d");
	}

	protected static String formatNumber(int number, String format) {
		return String.format(format, number);
	}

	public String getFunctionIdentifier() {
		return functionIdentifier;
	}

	public void setFunctionIdentifier(String functionIdentifier) {
		this.functionIdentifier = functionIdentifier;
	}
	
	public PerformanceTimer stop() {
		stopTime = System.currentTimeMillis();
		return this;
	}
	
	public String getTime() {
		long time = stopTime != null ? stopTime : System.currentTimeMillis();
		return ("function: " + functionIdentifier + " - time needed: " + formatTime(time - startTime));
	}
}
