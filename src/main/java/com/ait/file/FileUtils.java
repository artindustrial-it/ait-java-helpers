package com.ait.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {
	private static final String DEFAULT_SEPARATOR = "_";
	List<String> fileList;

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *          input zip file
	 * @param output
	 *          zip file output folder
	 */
	public static List<File> unzip(File zipFile) {
		List<File> result = new ArrayList<File>();
		try {
			// get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();
			while (ze != null) {
				result.add(writeTempFile(ze.getName(), zis));
				ze = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static File writeTempFile(String fileName, InputStream content) throws IOException {
		int indexExtension = fileName.lastIndexOf(".");
		String name = fileName.substring(0, indexExtension) + "_";
		String extension = fileName.substring(indexExtension);
		return writeTempFile(name, extension, content);
	}

	public static File writeTempFile(String prefix, String suffix,
			InputStream content) throws IOException {
		byte[] buffer = new byte[1024];

		File newFile = createTempFile(prefix, suffix);
		new File(newFile.getParent()).mkdirs();

		FileOutputStream fos = new FileOutputStream(newFile);

		int len;

		while ((len = content.read(buffer)) > 0) {
			fos.write(buffer, 0, len);
		}

		fos.close();
		return newFile;
	}

	public static File createTempFile(String prefix, String suffix) {
		return createTempFile(prefix, suffix, DEFAULT_SEPARATOR);
	}

	public static File createTempFile(String prefix, String suffix,
			String separator) {
		String tempDir = System.getProperty("java.io.tmpdir");
		String fileName = getStringWithSeparator(prefix, separator, false)
				+ System.nanoTime() + getStringWithSeparator(suffix, separator, true);
		return new File(tempDir, fileName);
	}

	private static String getStringWithSeparator(String string, String separator,
			boolean separatorBeforeString) {
		if (string != null && string.length() > 0) {
			StringBuilder sb = new StringBuilder();
			// print separator if it should be before string & string does not start with it
			if (separatorBeforeString && !string.startsWith(separator)) {
				sb.append(separator);
			}
			sb.append(string);
			// print separator if it should be after string & string does not ends with it
			if (!separatorBeforeString && !string.endsWith(separator)) {
				sb.append(separator);
			}
			
			return sb.toString();
		}
		return "";
	}

}