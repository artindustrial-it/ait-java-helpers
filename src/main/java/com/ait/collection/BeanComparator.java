package com.ait.collection;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.ait.clazz.ClassUtils;
import com.ait.converter.Converter;
import com.ait.converter.ConverterUtils;

public class BeanComparator implements Comparator<Object> {

	private List<Property> fields;
	public static Property A_TO_Z_PROPERTY = Property.of("A_TO_Z_PROPERTY");

	public BeanComparator(Property... sortProperties) {
		this.fields = Arrays.asList(sortProperties);
		if (sortProperties == null || sortProperties.length == 0) {
			throw new IllegalArgumentException(
					"You have to define at least one property field to sort by.");
		}
	}

	public int compare(Object o1, Object o2) {
		return (o1 == null) ? -1 : ((o2 == null) ? 1 : internalCompare(o1, o2, 0));
	}

	public int internalCompare(Object o1, Object o2, int i) {
		if (i == 0 && fields.get(0).equals(A_TO_Z_PROPERTY)) {
			this.fields = ConverterUtils.convert(ClassUtils.getAllGetterMethods(o1),
					new Converter<Method, Property>() {
						public Property convert(Method object) {
							return Property.of(object.getName());
						}
					});
		}

		Method getter = getGetter(i, o1);

		int result = 0;
		try {
			if (getter != null) {
				Object c1 = getter.invoke(o1);
				Object c2 = getter.invoke(o2);
				if (c1 != null || c2 != null) {
					if (c1 == null) {
						result = -1;
					} else if (c2 == null) {
						result = 1;
					} else {
						result = ((Comparable<Object>) c1).compareTo(c2);
					}
				}
				// chaining the sorting
				if (result == 0) {
					result = internalCompare(o1, o2, i + 1);
				} else if (!isAscending(i)) {
					result *= -1;
				}
			} else {
				// if we compared all defined fields without a result - the two
				// objects can not be further sorted
			}
		} catch (Exception e) {
			// If this exception occurs, then it is usually a fault of the developer.
			throw new RuntimeException("Cannot compare " + o1 + " with " + o2
					+ " on " + getter, e);
		}

		return result;
	}

	private Method getGetter(int i, Object o1) {
		if (i < fields.size()) {
			String propertyName = fields.get(i).getPropertyName();
			return ClassUtils.findGetterMethod(o1, propertyName);
		}
		return null;
	}

	private boolean isAscending(int i) {
		if (fields.size() > i) {
			return fields.get(i).isAsc();
		}
		return true;
	}

	public static class Property {
		private String propertyName;
		private boolean asc;

		private Property(String propertyName, boolean asc) {
			this.propertyName = propertyName;
			this.asc = asc;
		}

		public static Property of(String propertyName, boolean asc) {
			return new Property(propertyName, asc);
		}

		public static Property of(String propertyName) {
			return of(propertyName, true);
		}

		public String getPropertyName() {
			return propertyName;
		}

		public boolean isAsc() {
			return asc;
		}
	}
}