package com.ait.converter;

import org.testng.TestNG;
import org.testng.annotations.Test;

public class StringConverterTest extends TestNG {

	@Test
	public void testConversionOfObject() {
		String actual = new StringConverter<Asdf>().convert(new Asdf());
		String lineSeparator = System.getProperty("line.separator");
		String expected = "Asdf" + lineSeparator
				+ "    getTestString: getTestString" + lineSeparator
				+ "    getTestVoid: null";
//		Assert.assertEquals(expected, actual);
	}

	public static class Asdf {
		public String getTestString() {
			return "getTestString";
		}

		public void getTestVoid() {
			int i = 0;
			i++;
		}

		public String getTestStringWithParams(String input) {
			return "getTestStringWithParams: " + input;
		}
	}
}
