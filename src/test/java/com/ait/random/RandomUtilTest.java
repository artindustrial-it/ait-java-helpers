package com.ait.random;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.Test;

public class RandomUtilTest extends TestNG {

	@Test
	public void getRandom() {
		String[] items = new String[] {"1", "2", "3", "4"};
		
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < 5000 && result.size() < items.length; i++) {
			String rand = RandomUtil.getRandom(items);
			if (!result.contains(rand)) {
				result.add(rand);
			}
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
//		for (String item : result) {
//			System.out.println("actual: " + item);
//		}
		Assert.assertEquals(result.size(), items.length);
	}
}
