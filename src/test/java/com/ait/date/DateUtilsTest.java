package com.ait.date;

import java.util.Date;

import junit.framework.Assert;

import org.testng.TestNG;
import org.testng.annotations.Test;

public class DateUtilsTest extends TestNG {

	@Test
	public void testParse1() {
		String dateString = "28.02.1016";
		Date date = DateUtils.parseDate(dateString);
		Assert.assertEquals(dateString, DateUtils.getFormattedDate(date, DateUtils.DEFAULT_FORMAT));
	}

	@Test
	public void testParse2() {
		Date date = DateUtils.parseDate("31.02.1016");
		Assert.assertEquals(null, date);
	}
}
