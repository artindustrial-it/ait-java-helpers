package com.ait.clazz;

import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.Test;

public class ClassUtilsTest extends TestNG {

	@Test
	public static void testGetMethodName() {
		String actual = ClassUtils.getMethodName();
		Assert.assertEquals(actual, "testGetMethodName");
	}

	@Test
	public static void testGetMethodNameWithDepth() {
		String actual = ClassUtils.getMethodName(1);
		Assert.assertEquals(actual, "invoke0");
	}

	@Test
	public static void testGetClassName() {
		String actual = ClassUtils.getClassName();
		Assert.assertEquals(actual, ClassUtilsTest.class.getCanonicalName());
	}
}
