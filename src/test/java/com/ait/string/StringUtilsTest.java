package com.ait.string;

import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.TestNG;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringUtilsTest extends TestNG {

	@Test
	public void testCountOccurrencesWithAChar() {
		int actual = StringUtils.countOccurrences("asdfasdfasdf", "a");
		Assert.assertEquals(actual, 3);
	}

	@Test
	public void testCountOccurrencesWithAString() {
		int actual = StringUtils.countOccurrences("asdfasdfasdfadfsasasdfdfdfdf",
				"as");
		Assert.assertEquals(actual, 5);
	}

	@Test(dataProvider = "join")
	public void testJoin(String[] join, String separator, String expected) {
		String actual = StringUtils.join(separator, join);
		Assert.assertEquals(actual, expected, "Der gejointete String sollte '"
				+ expected + "' sein, ist aber '" + actual + "'");
	}

	@DataProvider(name = "join")
	public Object[][] provideDataJoin() {

		// @formatter:off
		return new Object[][] { 
				{ null, ";", null },
				{ new String[] {"ASDF"}, ";", "ASDF" },
				{ new String[] {"AS", "DF"}, ";", "AS;DF" },
				{ new String[] {""}, ";", "" },
				{ new String[] {"", ""}, ";-)", ";-)" },
				{ new String[] {"A-S", "DF"}, "-", "A-S-DF" },
				{ new String[] {"A-S", "DF"}, "--", "A-S--DF" }
		};
		// @formatter:on
	}

	@Test(dataProvider = "split")
	public void testSplit(String split, String separator, List<String> expected) {
		List<String> actual = StringUtils.split(split, separator);
		Assert.assertEquals(
				actual.size(),
				expected.size(),
				"Die Länge des gesplitteten Strings '" + split + "' sollte "
						+ expected.size() + " sein, aber sie ist " + actual.size());
		for (int i = 0; i < actual.size(); i++) {
			Assert.assertEquals(actual.get(i), expected.get(i),
					"Der gesplittete String '" + split + "' sollte an der Stelle " + i
							+ " '" + expected.get(i) + "' sein, ist aber '" + actual.get(i)
							+ "'");
		}
	}

	@DataProvider(name = "split")
	public Object[][] provideDataSplit() {

		// @formatter:off
		return new Object[][] { 
				{ null, ";", Arrays.asList(new String[] {}) },
				{ "ASDF", ";", Arrays.asList(new String[] { "ASDF" }) },
				{ "AS;DF", ";", Arrays.asList(new String[] { "AS", "DF" }) },
				{ "", ";", Arrays.asList(new String[] { "" }) },
				{ "A-S--DF", "--", Arrays.asList(new String[] { "A-S", "DF" }) }

		};
		// @formatter:on
	}

	@Test
	public void testSplitListModifiable() {
		List<String> actual = StringUtils.split("asdf,ASDF,asdf", ",");
		actual.add("ASDF");
		List<String> expected = Arrays.asList(new String[] { "asdf", "ASDF",
				"asdf", "ASDF" });
		Assert.assertEquals(actual.size(), expected.size(),
				"Die Länge des gesplitteten Strings  sollte " + expected.size()
						+ " sein, aber sie ist " + actual.size());
		for (int i = 0; i < actual.size(); i++) {
			Assert.assertEquals(actual.get(i), expected.get(i),
					"Der gesplittete String sollte an der Stelle " + i
							+ " '" + expected.get(i) + "' sein, ist aber '" + actual.get(i)
							+ "'");
		}
	}
}
