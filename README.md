ait-java-helpers
=====================

About:
------

The ait java helpers are often needed common functions / helpers / utils such as DateUtils, FileUtils,...

License:
--------

This software project is licensed under the Apache Software Foundation license, version 2.0.

When should a function be added here:
-------------------------------------

Add if your function... 

* is ready for production (or javadoc that it is not meant for production and why)
* adds value / saves writing boilerplate code again and again
* is reusable
* is general purpose
* does not contain any domain / business specific code / logic
* well documented so any developer can use it without troubles or looking into the code for hours

Comments:
---------